/*
 * ADC.h
 *
 * Created: 2014-08-17 14:34:39
 *  Author: jerzyk
 */ 


#ifndef ADC_H_
#define ADC_H_

#include <avr/io.h>
#include <avr/interrupt.h>
//volatile uint16_t ADC_Result = 0;
char ADC_Result[4];

void ADC_Enable(void);
void ADC_EnableInterrupts(void);
uint16_t ADC_StartConvertion(uint8_t chanel);
void ADC_Start(void);


#endif /* ADC_H_ */