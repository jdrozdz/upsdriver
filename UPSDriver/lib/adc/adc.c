/*
 * Config.h
 *
 * Created: 2014-08-17 14:24:51
 *  Author: Jerzy Dro�d� interSHOCK Systemy Informatyczne
 */ 

#include "../config.h"
#include "adc.h"

/// Enable ADC
void ADC_Enable(void){
	ADMUX |= (1 << REFS0) | (1 << REFS1);
	ADCSRA=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0); // Prescaler 128
}

void ADC_EnableInterrupts(void){
	ADCSRA |= (1 << ADIE) | (1 << ADEN);
}
//==============================
uint16_t ADC_StartConvertion(uint8_t chanel){
	
	chanel = (chanel & 0b00000111);
	ADMUX |= chanel;
	
	ADCSRA |= (1<<ADSC);
	 
	while(!(ADCSRA & (1<<ADIF)));
	
	ADCSRA|=(1<<ADIF);
	
	return ADC;
}

void ADC_Start(){
	ADCSRA |= (1<<ADSC);
}
