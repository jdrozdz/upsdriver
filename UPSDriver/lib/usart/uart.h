/*
 * uart.h
 *
 * Created: 2014-08-19 21:14:54
 *  Author: Jerzy Dro�d� interSHOCK Systemy Informatyczne
 */ 


#ifndef UART_H_
#define UART_H_

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdlib.h>

void USART_Init(uint16_t baud);
void USART_Transmit(uint8_t data);
void USART_SendString(const char *s);
uint8_t USART_Receive(void);
void USART_ClearScreen(void);
void USART_GoTo(uint8_t x, uint8_t y);
void USART_ResetAtr(void);
void USART_FontColor(uint8_t txt);
void USART_BGColor(uint8_t bg);
void USART_PutInt(uint16_t val, uint8_t radix);
#endif /* UART_H_ */