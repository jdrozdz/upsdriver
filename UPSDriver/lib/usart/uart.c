/*
 * uart.c
 *
 * Created: 2014-08-19 21:21:18
 *  Author: Jerzy Dro�d� interSHOCK Systemy Informatyczne
 */ 

#include "uart.h"

void USART_Init(uint16_t baud){
   UBRRL = (unsigned char) baud;
   UBRRH = ((unsigned char) baud >> 8);
  UCSRB = ((1<<TXEN)|(1<<RXEN) | (1<<RXCIE));
}


void USART_Transmit(uint8_t data){
  while((UCSRA &(1<<UDRE)) == 0);
  UDR = data;
}

uint8_t USART_Receive(){
  while((UCSRA &(1<<RXC)) == 0);
  return UDR;
}

void USART_SendString(const char *s){
	while(*s){
		USART_Transmit(*s++);
		_delay_ms(30);
	}
}

void USART_PutInt(uint16_t val, uint8_t radix){
	char buf[17];
	itoa(val, buf, radix);
	USART_SendString(buf);
}

void USART_ClearScreen(void){
	USART_Transmit(0x1b);
	USART_Transmit('[');
	USART_Transmit('H');
	USART_Transmit(0x1b);
	USART_Transmit('[');
	USART_Transmit('2');
	USART_Transmit('J');
}
void USART_GoTo(uint8_t x, uint8_t y){
	USART_Transmit(0x1b);
	USART_Transmit('[');
	USART_PutInt(x, 10);
	USART_Transmit(';');
	USART_PutInt(y, 10);
	USART_Transmit('H');	
}
void USART_ResetAtr(void){
	USART_Transmit(0x1b);
	USART_Transmit('[');
	USART_Transmit(0);
	USART_Transmit('m');
}
void USART_FontColor(uint8_t txt){
	USART_Transmit(0x1b);
	USART_Transmit('[');
	USART_Transmit('4');
	USART_Transmit(txt+'0');
	USART_Transmit('m');
}

void USART_BGColor(uint8_t bg){
	USART_Transmit(0x1b);
	USART_Transmit('[');
	USART_Transmit('3');
	USART_Transmit(bg+'0');
	USART_Transmit('m');
}