/*
 * Config.h
 *
 * Created: 2014-08-17 14:24:51
 *  Author: Jerzy Dro�d� interSHOCK Systemy Informatyczne
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

#define SUPP DDRA
#define SUPP_PORT PORTA
#define SUPP_PIN 2
#define SUPP_ADC ADC1
#define ADC_PORT SUPP_PORT


#define AC DDRD
#define AC_PORT PORTD
#define AC_PIN 3
#define AC_ADC ADC0

#endif /* CONFIG_H_ */