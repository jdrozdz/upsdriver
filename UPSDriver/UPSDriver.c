/*
 * UPSDriver.c
 *
 * Created: 2014-08-17 14:22:19
 *  Author: Jerzy Drozdz interSHOCK Systemy Informatyczne
 */

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lib/usart/uart.h"
#include "lib/adc/adc.h"

int main(void){
   USART_Init(103);  // Initialize USART
   ADC_Enable();
   ADC_EnableInterrupts();
   
   sei();
   
   ADC_Start();

   while(1){ }
}

ISR(ADC_vect){
	uint16_t high = ADCH;
	uint8_t low = ADCL;
	USART_GoTo(0,0);
	USART_PutInt(high, 10);
	USART_SendString(" | ");
	USART_PutInt(low, 10);
	USART_SendString("               ");
	ADC_Start();
}